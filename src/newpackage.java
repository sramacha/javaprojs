import com.satish.helloworld.*;

import java.io.*;
import java.util.*;

import org.junit.Test;



public class newpackage {
    public static void main(String[] args)
    {
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.print_hello();
        List<Integer> my_list = create_list();
        HashMap<String,Integer> my_dict = new HashMap<String,Integer>();
        System.out.println("Printing List items\n");
        my_list.forEach(item -> System.out.println(item));
        System.out.println("Printing Dict items\n");
        my_dict = create_dict();
        for (String i : my_dict.keySet()) {
            System.out.println("Name: " + i + " Age: " + my_dict.get(i));
        }


    }

    public static List<Integer> create_list()
    {
        //To read value in IDE since system.console().readline() doesn't work since IDE uses Javaw executable to run instead Java
        Scanner br = new Scanner(System.in);
        List<Integer> my_list = new ArrayList<Integer>();
        System.out.println("Enter the number of items in the list\n");
        Integer number = Integer.parseInt(br.next());
        //Integer number = 5;
        for(Integer i=0;i<number;i++)
        {
            System.out.println("Enter the list item\n");
            my_list.add( Integer.parseInt(br.next()));
         //   my_list.add( i);
        }
        return my_list;

    }

    public static HashMap<String,Integer> create_dict()
    {
        HashMap<String,Integer> my_dict = new HashMap<String,Integer>();
        my_dict.put("Satish",42);
        my_dict.put("Lochan",7);
        my_dict.put("Rajani", 35);
        return my_dict;
    }


}
